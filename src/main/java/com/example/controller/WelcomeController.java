package com.example.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class WelcomeController {



    //config in application.properties
    @Value("${welcome.message}")
    private String message;


    @GetMapping("/")
    public String welcome(Model model) {
        model.addAttribute("message", message);
        return "Hello world"; //view
    }


}